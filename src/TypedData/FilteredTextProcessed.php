<?php

namespace Drupal\agoraenum\TypedData;

use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\text\TextProcessed;

/**
 * A computed property for processing text with a format for agoraenum fields.
 */
class FilteredTextProcessed extends TextProcessed {

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);

    if ($definition->getSetting('format source') === NULL) {
      throw new \InvalidArgumentException("The definition's 'format source' key has to specify the name of the format property to be processed.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->processed !== NULL) {
      return $this->processed;
    }

    $item = $this->getParent();
    $text = $item->{($this->definition->getSetting('text source'))};
    $format = $item->{($this->definition->getSetting('format source'))};

    // Avoid running check_markup() on empty strings.
    if (!isset($text) || $text === '') {
      $this->processed = '';
    }
    else {
      $this->processed = check_markup($text, $format, $item->getLangcode());
    }
    return $this->processed;
  }

}
