<?php

namespace Drupal\agoraenum\tmgmt;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\Element;
use Drupal\tmgmt_content\DefaultFieldProcessor;

/**
 * Field processor for the link field.
 */
class AgoraenumFieldProcessor extends DefaultFieldProcessor {

  /**
   * {@inheritdoc}
   */
  public function extractTranslatableData(FieldItemListInterface $field) {
    $data = parent::extractTranslatableData($field);
    foreach (Element::children($data) as $delta) {
      if (!empty($data[$delta]['icon']['#translate'])) {
        $data[$delta]['icon']['#translate'] = FALSE;
      }
    }

    return $data;
  }

}
