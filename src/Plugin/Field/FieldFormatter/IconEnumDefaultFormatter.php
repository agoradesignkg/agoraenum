<?php

namespace Drupal\agoraenum\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'icon_enum_default' formatter.
 *
 * @FieldFormatter(
 *   id = "icon_enum_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "icon_enum",
 *   },
 * )
 */
class IconEnumDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'agoraenum_icon',
        '#item' => $item,
      ];
    }

    return $element;
  }

}
