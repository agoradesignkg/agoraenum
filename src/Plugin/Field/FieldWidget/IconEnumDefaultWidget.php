<?php

namespace Drupal\agoraenum\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'icon_enum' widget.
 *
 * @FieldWidget(
 *   id = "icon_enum_default",
 *   label = @Translation("Enumeration item (with icon)"),
 *   field_types = {
 *     "icon_enum"
 *   },
 * )
 */
class IconEnumDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Static options cache.
   *
   * @var array|null
   */
  protected ?array $options = NULL;

  /**
   * Constructs a IconEnumDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, AccountInterface $current_user, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['title'] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->title ?? NULL,
      '#size' => 60,
      '#placeholder' => '',
      '#maxlength' => 255,
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
      '#access' => empty($this->getFieldSetting('hide_title')),
    ];

    $element['text'] = [
      '#title' => $this->t('Text'),
      '#type' => 'text_format',
      '#base_type' => 'textarea',
      '#default_value' => $items[$delta]->text_value,
      '#rows' => 5,
      '#placeholder' => '',
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
      '#format' => $items[$delta]->text_format,
      '#access' => empty($this->getFieldSetting('hide_text')),
    ];

    // @todo validateElement like OptionsWidgetBase does, but maybe we do not need this.
    $element['icon'] = [
      '#title' => $this->t('Icon'),
      '#type' => 'select',
      '#options' => $this->getIconOptions($items->getEntity()),
      '#default_value' => $items[$delta]->icon ?? NULL,
    ];

    return $element;
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getIconOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {
      // Limit the settable options for the current user account.
      $options = $this->fieldDefinition
        ->getFieldStorageDefinition()
        ->getOptionsProvider('icon', $entity)
        ->getSettableOptions($this->currentUser);

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $options = ['_none' => $empty_label] + $options;
      }

      $context = [
        'fieldDefinition' => $this->fieldDefinition,
        'entity' => $entity,
        'currentUser' => $this->currentUser,
      ];
      $this->moduleHandler->alter('agoraenum_icon_options', $options, $context);

      array_walk_recursive($options, [$this, 'sanitizeLabel']);

      $this->options = $options;
    }
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    if ($violation->arrayPropertyPath == ['format'] && isset($element['format']['#access']) && !$element['format']['#access']) {
      // Ignore validation errors for formats if formats may not be changed,
      // i.e. when existing formats become invalid. See filter_process_format().
      return FALSE;
    }
    return $element;
  }

  /**
   * Sanitizes a string label to display as an option.
   *
   * @param string $label
   *   The label to sanitize.
   */
  protected function sanitizeLabel(&$label) {
    $label = Html::decodeEntities(strip_tags($label));
  }

  /**
   * Returns the empty option label to add to the list of options, if any.
   *
   * @return string
   *   A label of the empty option.
   */
  protected function getEmptyLabel() {
    return $this->t('- None -');
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $new_values = [];
    foreach ($values as $delta => &$value) {
      $new_value = $value;
      $new_value['text_value'] = $value['text']['value'];
      $new_value['text_format'] = $value['text']['format'];
      unset($new_value['text']);
      $new_values[$delta] = $new_value;
    }
    return $new_values;
  }

}
